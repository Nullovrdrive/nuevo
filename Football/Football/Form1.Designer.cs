﻿namespace Football
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvroja = new System.Windows.Forms.DataGridView();
            this.dgvamarilla = new System.Windows.Forms.DataGridView();
            this.dgvplayer = new System.Windows.Forms.DataGridView();
            this.dgvequipos = new System.Windows.Forms.DataGridView();
            this.btnver = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvroja)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvamarilla)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvplayer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvequipos)).BeginInit();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(116, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(640, 62);
            this.label5.TabIndex = 19;
            this.label5.Text = "World Cup 2010 tournament";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::Football.Properties.Resources.logo;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(98, 65);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(484, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(272, 76);
            this.label4.TabIndex = 17;
            this.label4.Text = "Tarjetas Amarillas";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(483, 321);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(273, 64);
            this.label3.TabIndex = 16;
            this.label3.Text = "Tarjetas Rojas";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(292, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(186, 76);
            this.label2.TabIndex = 15;
            this.label2.Text = "Jugadores";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(12, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(274, 76);
            this.label1.TabIndex = 14;
            this.label1.Text = "Equipos";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dgvroja
            // 
            this.dgvroja.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvroja.Location = new System.Drawing.Point(483, 388);
            this.dgvroja.Name = "dgvroja";
            this.dgvroja.Size = new System.Drawing.Size(273, 137);
            this.dgvroja.TabIndex = 13;
            // 
            // dgvamarilla
            // 
            this.dgvamarilla.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvamarilla.Location = new System.Drawing.Point(484, 159);
            this.dgvamarilla.Name = "dgvamarilla";
            this.dgvamarilla.Size = new System.Drawing.Size(272, 159);
            this.dgvamarilla.TabIndex = 12;
            // 
            // dgvplayer
            // 
            this.dgvplayer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvplayer.Location = new System.Drawing.Point(292, 159);
            this.dgvplayer.Name = "dgvplayer";
            this.dgvplayer.Size = new System.Drawing.Size(185, 366);
            this.dgvplayer.TabIndex = 11;
            // 
            // dgvequipos
            // 
            this.dgvequipos.AllowUserToDeleteRows = false;
            this.dgvequipos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvequipos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.btnver});
            this.dgvequipos.Location = new System.Drawing.Point(12, 159);
            this.dgvequipos.Name = "dgvequipos";
            this.dgvequipos.ReadOnly = true;
            this.dgvequipos.Size = new System.Drawing.Size(274, 366);
            this.dgvequipos.TabIndex = 10;
            this.dgvequipos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvequipos_CellClick);
            // 
            // btnver
            // 
            this.btnver.HeaderText = "Detalles";
            this.btnver.Name = "btnver";
            this.btnver.ReadOnly = true;
            this.btnver.Text = "Ver";
            this.btnver.UseColumnTextForButtonValue = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(768, 537);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvroja);
            this.Controls.Add(this.dgvamarilla);
            this.Controls.Add(this.dgvplayer);
            this.Controls.Add(this.dgvequipos);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvroja)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvamarilla)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvplayer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvequipos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvroja;
        private System.Windows.Forms.DataGridView dgvamarilla;
        private System.Windows.Forms.DataGridView dgvplayer;
        private System.Windows.Forms.DataGridView dgvequipos;
        private System.Windows.Forms.DataGridViewButtonColumn btnver;
    }
}

