﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Football.eu.dataaccess.footballpool;

namespace Football
{
    public partial class Form1 : Form
    {
        Info servicios;
        tTeamInfo[] equipo;
        tPlayerNames[] jugadores;

       

        public Form1()
        {
            InitializeComponent();
        }

        //private void Form1_Load(object sender, EventArgs e)
        //{
            
        //}
        public void cargarEquipos()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Nombre", typeof(string));
            try
            {
                servicios = new Info();
                equipo = servicios.Teams();
                //servicios.Teams();

                for (int i = 0; i < equipo.Length; i++)
                {
                    dt.Rows.Add(equipo[i].sName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR  " + ex);
            }
            dgvequipos.DataSource = dt;
        }

       
        public void buscarjugadores(string Equipo)
        {
            DataTable jugador = new DataTable();

            jugador.Columns.Add("Nombre", typeof(string));
            try
            {
                servicios = new Info();
                jugadores = servicios.AllPlayerNames(true);
                for (int i = 0; i < jugadores.Length; i++)
                {
                    if (jugadores[i].sCountryName == Equipo)
                    {
                        jugador.Rows.Add(jugadores[i].sName.ToString());
                    }
                }
                dgvplayer.DataSource = jugador;

            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR  " + ex);
            }
        }
        public void buscartargeta(string tarjeta)
        {
            DataTable card = new DataTable();
            tPlayersWithCards[] cards;

            card.Columns.Add("Nombre", typeof(string));
            card.Columns.Add("Numero", typeof(int));
            try
            {
                servicios = new Info();
                cards = servicios.AllPlayersWithYellowCards(true, true);
                for (int i = 0; i < cards.Length; i++)
                {
                    if (cards[i].sTeamName == tarjeta)
                    {
                        card.Rows.Add(cards[i].sName.ToString(), cards[i].iYellowCards);
                    }
                }
                dgvamarilla.DataSource = card;

            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR  " + ex);
            }
        }
        public void buscartargeta2(string tarjeta)
        {
            DataTable card2 = new DataTable();
            tPlayersWithCards[] cards;

            card2.Columns.Add("Nombre", typeof(string));
            card2.Columns.Add("Numero", typeof(int));
            try
            {
                servicios = new Info();
                cards = servicios.AllPlayersWithRedCards(true, true);
                for (int i = 0; i < cards.Length; i++)
                {
                    if (cards[i].sTeamName == tarjeta)
                    {
                        card2.Rows.Add(cards[i].sName.ToString(), cards[i].iRedCards);
                    }
                }
                dgvroja.DataSource = card2;

            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR  " + ex);
            }
        }

        private void dgvequipos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.dgvequipos.Columns[e.ColumnIndex].Name == "btnver")
            {
                DataGridViewRow row = dgvequipos.Rows[e.RowIndex];
                string ciudad = row.Cells["Nombre"].Value.ToString();
                buscarjugadores(ciudad);
                buscartargeta(ciudad);
                buscartargeta2(ciudad);
            }
        }

        private void Form1_Load_1(object sender, EventArgs e)
        {
            cargarEquipos();
        }
        

    }
}
